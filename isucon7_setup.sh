#!/bin/bash

# 帯域制限:
# sudo tc qdisc add dev enp0sp root handle 1:0 tbf rate 500mbit burst 25kb limit 250kb
# sudo tc qdisc del dev enp0s9 root handle 1:0 tbf rate 500mbit burst 25kb limit 250kb

create_user() {
	if [ "$EUID" -ne 0 ]; then # check sudo
		echo "Error: Please run as root" >&2
		exit 1
	fi

	useradd isucon
	su -c "echo -e 'isucon\\tALL=(ALL)\\tNOPASSWD: ALL' > /etc/sudoers.d/isucon && chmod 440 /etc/sudoers.d/isucon"
	cp -r /home/ubuntu /home/isucon
	chown -R isucon:isucon /home/isucon
	exec $0
}

create_swap() {
	# size of swapfile
	swapsize=4G
	if grep -q "swapfile" /etc/fstab
	then
		# if there is swap, skip it
		return 0
	else
		# if not create it
		echo 'swapfile not found. Adding swapfile.'
		sudo fallocate -l ${swapsize} /swapfile
		sudo chmod 600 /swapfile
		sudo mkswap /swapfile
		sudo swapon /swapfile
		sudo su -c "echo '/swapfile none swap defaults 0 0' >> /etc/fstab"
	fi
}

provisioning() {
	if [ "$(whoami)" != "isucon" ]
	then
		# redo as isucon, if I am not the isucon user
		exec sudo su isucon -c $0
	fi

	cd $HOME

	echo '### PROVISION ###'
	sudo DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends git curl mysql-server nginx make
	git clone https://github.com/isucon/isucon7-qualify.git isubata

	echo '### golang ###'
	curl https://bitbucket.org/tailed/isucon/raw/adfd611c5483293cddb19bdda98234fbf6245b32/install.sh | bash -s
	export PATH=$HOME/go/bin:$PATH
	go get github.com/constabulary/gb/...
	cd ~/isubata/bench
	gb vendor restore
	make
	./bin/gen-initial-dataset
	
	cd ~/isubata/webapp/go
	make
	sudo cp ~/isubata/files/app/isubata.golang.service /etc/systemd/system/
	cp ~/isubata/files/app/env.sh ~/env.sh
	chmod 755 ~/env.sh
	sudo systemctl --now enable isubata.golang.service

	echo '### /etc/hosts ###'
	sudo su -c 'echo "192.168.33.11 app1 app1" >> /etc/hosts'
	sudo su -c 'echo "192.168.33.12 app2 app2" >> /etc/hosts'
	sudo su -c 'echo "192.168.33.13 app3 app3 db00" >> /etc/hosts'

	echo '### mysql ###'
	sudo ~/isubata/db/init.sh
	zcat ~/isubata/bench/isucon7q-initial-dataset.sql.gz | sudo mysql --default-character-set=utf8 isubata
	sudo mysql <<EOF
	CREATE USER isucon@'%' IDENTIFIED BY 'isucon';
	GRANT ALL on *.* TO isucon@'%';
	CREATE USER isucon@'localhost' IDENTIFIED BY 'isucon';
	GRANT ALL on *.* TO isucon@'localhost';
EOF
	sed -e 's/^bind-address.*$/bind-address = 0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf > /tmp/mysqld.cnf && sudo mv /tmp/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
	sudo service mysql restart


	echo '### nginx ###'
	sudo cp ~/isubata/files/app/nginx.* /etc/nginx/sites-available
	cd /etc/nginx/sites-enabled
	sudo unlink default
	sudo ln -s ../sites-available/nginx.conf  # php の場合は nginx.php.conf
	sudo systemctl restart nginx
}

set -eux

create_swap

if grep isucon /etc/passwd >/dev/null
then
	provisioning
else
	create_user
fi


