#!/bin/bash

cd $(dirname $0)
sudo su isucon -c "
cd /home/isucon/isubata/bench/
./bin/bench -remotes=app1,app2 -output=result.json
"
filename=/vagrant/log/$(/vagrant/logger.sh lastid)/bench.txt
jq . </home/isucon/isubata/bench/result.json > $filename
less $filename
