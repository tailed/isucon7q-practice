#!/usr/bin/sudo /bin/bash

cd $(dirname $0)
source ./init_header.sh

echo '### nginx ###'
update_if_differ nginx /vagrant///etc/nginx/sites-enabled/nginx.conf /etc/nginx/sites-enabled/nginx.conf
update_if_differ nginx /vagrant///etc/nginx/mime.types /etc/nginx/mime.types
update_if_differ nginx /vagrant///etc/nginx/nginx.conf /etc/nginx/nginx.conf
ensure_nginx_syntax
restart_service_if_updated nginx

echo '### mysql ###'
update_if_differ mysql /vagrant///etc/mysql/mysql.conf.d/mysqld_safe_syslog.cnf /etc/mysql/mysql.conf.d/mysqld_safe_syslog.cnf
update_if_differ mysql /vagrant///etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
update_if_differ mysql /vagrant///etc/mysql/conf.d/mysqldump.cnf /etc/mysql/conf.d/mysqldump.cnf
update_if_differ mysql /vagrant///etc/mysql/conf.d/mysql.cnf /etc/mysql/conf.d/mysql.cnf
update_if_differ mysql /vagrant///etc/mysql/my.cnf /etc/mysql/my.cnf
ensure_mysql_syntax
restart_service_if_updated mysql

echo '### isubata.golang.service ###'
(
	cd ./go
	make
)
if [ "$HOSTNAME" != "app3" ]
then
	service mysql stop
fi
update_if_differ isubata.golang.service ./public /home/isucon/isubata/webapp/public
update_if_differ isubata.golang.service ./go/ /home/isucon/isubata/webapp/go/
update_if_differ isubata.golang.service /vagrant///etc/systemd/system/isubata.golang.service /etc/systemd/system/isubata.golang.service
systemctl --quiet is-enabled isubata.golang.service || ( set -x; systemctl --now enable isubata.golang.service )
restart_service_if_updated isubata.golang.service

echo '### other ###'
update_if_differ other ./etc/security/limits.conf /etc/security/limits.conf
update_if_differ other ./etc/sysctl.conf /etc/sysctl.conf

