package main

import (
	crand "crypto/rand"
	"crypto/sha1"
	"database/sql"
	"encoding/binary"
	"encoding/gob"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"text/template"
	"time"

	cluster "bitbucket.org/tailed/golang/rpccluster"
	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
	_ "github.com/labstack/echo/middleware"
)

const (
	avatarMaxBytes = 1 * 1024 * 1024
)

var (
	db            *sqlx.DB
	ErrBadReqeust = echo.NewHTTPError(http.StatusBadRequest)
)

func FileExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

type Renderer struct {
	templates *template.Template
}

func (r *Renderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return r.templates.ExecuteTemplate(w, name, data)
}

func init() {
	seedBuf := make([]byte, 8)
	crand.Read(seedBuf)
	rand.Seed(int64(binary.LittleEndian.Uint64(seedBuf)))

	db_host := os.Getenv("ISUBATA_DB_HOST")
	if db_host == "" {
		db_host = "127.0.0.1"
	}
	db_port := os.Getenv("ISUBATA_DB_PORT")
	if db_port == "" {
		db_port = "3306"
	}
	db_user := os.Getenv("ISUBATA_DB_USER")
	if db_user == "" {
		db_user = "root"
	}
	db_password := os.Getenv("ISUBATA_DB_PASSWORD")
	if db_password != "" {
		db_password = ":" + db_password
	}

	dsn := fmt.Sprintf("%s%s@tcp(%s:%s)/isubata?parseTime=true&loc=Local&charset=utf8mb4",
		db_user, db_password, db_host, db_port)

	log.Printf("Connecting to db: %q", dsn)
	db, _ = sqlx.Connect("mysql", dsn)
	for {
		err := db.Ping()
		if err == nil {
			break
		}
		log.Println(err)
		time.Sleep(time.Second * 3)
	}

	db.SetMaxOpenConns(20)
	db.SetConnMaxLifetime(5 * time.Minute)
	log.Printf("Succeeded to connect db.")

	initializeMessageCache()
	initializeUserCache()
	initializeChannelCache()
	initializeHavereadCache()
}

type User struct {
	ID          int64     `json:"-" db:"id"`
	Name        string    `json:"name" db:"name"`
	Salt        string    `json:"-" db:"salt"`
	Password    string    `json:"-" db:"password"`
	DisplayName string    `json:"display_name" db:"display_name"`
	AvatarIcon  string    `json:"avatar_icon" db:"avatar_icon"`
	CreatedAt   time.Time `json:"-" db:"created_at"`
}

var idToUsers = new(sync.Map)
var nameToUserID = new(sync.Map)

func initializeUserCache() {
	idToUsers = new(sync.Map)
	nameToUserID = new(sync.Map)

	users := []User{}
	err := db.Select(&users, "SELECT * FROM user")
	if err != nil {
		panic(err)
	}
	for _, user := range users {
		idToUsers.Store(user.ID, user)
		nameToUserID.Store(user.Name, user.ID)
	}

}

func getUser(userID int64) (*User, error) {
	u, ok := idToUsers.Load(userID)
	if !ok {
		return nil, nil
	} else {
		user := u.(User)
		return &user, nil
	}
}

func addMessage(channelID, userID int64, content string) error {
	createdAt := time.Now()
	_, err := db.Exec(
		"INSERT INTO message (channel_id, user_id, content, created_at) VALUES (?, ?, ?, ?)",
		channelID, userID, content, createdAt)

	msg := Message{
		ID:        0,
		ChannelID: channelID,
		UserID:    userID,
		Content:   content,
		CreatedAt: createdAt,
	}
	mycluster.CallAll("addMsg", msg)
	return err
}
func init() {
	cluster.Register("addMsg", addMsg)
	gob.Register(Message{})
}
func addMsg(msg Message) {
	channelID := msg.ChannelID
	messagesMutex.Lock()
	defer messagesMutex.Unlock()
	msg.ID = int64(len(channelToMessages[channelID]) + 1)
	channelToMessages[channelID] = append(channelToMessages[channelID], msg)
}

type Message struct {
	ID        int64     `db:"id"`
	ChannelID int64     `db:"channel_id"`
	UserID    int64     `db:"user_id"`
	Content   string    `db:"content"`
	CreatedAt time.Time `db:"created_at"`
}

const MaxChannel = 1000

var channelToMessages = make([][]Message, MaxChannel)
var messagesMutex sync.RWMutex

func initializeMessageCache() {
	channelToMessages = make([][]Message, MaxChannel)
	messages := []Message{}
	err := db.Select(&messages, "SELECT * FROM message")
	if err != nil {
		panic(err)
	}
	for _, msg := range messages {
		addMsg(msg)
	}
}

func queryMessages(chanID, lastID int64) []Message {
	start := lastID + 1
	messagesMutex.RLock()
	defer messagesMutex.RUnlock()
	end := int64(len(channelToMessages[chanID]))
	if 100 <= end-start+1 {
		start = end - 100 + 1
	}
	if end-start+1 <= 0 {
		return []Message{}
	}
	msgs := make([]Message, end-start+1)
	for i, j := end, 0; i >= start; i, j = i-1, j+1 {
		msgs[j] = channelToMessages[chanID][i-1]
	}
	return msgs
}
func queryMessageHistory(chanID, start, limit int64) []Message {
	messagesMutex.RLock()
	defer messagesMutex.RUnlock()
	end := int64(len(channelToMessages[chanID]))
	if limit <= end-start+1 {
		start = end - limit + 1
	}
	if end-start+1 <= 0 {
		return []Message{}
	}
	msgs := make([]Message, end-start+1)
	for i, j := end, 0; i >= start; i, j = i-1, j+1 {
		msgs[j] = channelToMessages[chanID][i-1]
	}
	return msgs
}

func sessUserID(c echo.Context) int64 {
	sess, _ := session.Get("session", c)
	var userID int64
	if x, ok := sess.Values["user_id"]; ok {
		userID, _ = x.(int64)
	}
	return userID
}

func sessSetUserID(c echo.Context, id int64) {
	sess, _ := session.Get("session", c)
	sess.Options = &sessions.Options{
		HttpOnly: true,
		MaxAge:   360000,
	}
	sess.Values["user_id"] = id
	sess.Save(c.Request(), c.Response())
}

func ensureLogin(c echo.Context) (*User, error) {
	var user *User
	var err error

	userID := sessUserID(c)
	if userID == 0 {
		goto redirect
	}

	user, err = getUser(userID)
	if err != nil {
		return nil, err
	}
	if user == nil {
		sess, _ := session.Get("session", c)
		delete(sess.Values, "user_id")
		sess.Save(c.Request(), c.Response())
		goto redirect
	}
	return user, nil

redirect:
	c.Redirect(http.StatusSeeOther, "/login")
	return nil, nil
}

const LettersAndDigits = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func randomString(n int) string {
	b := make([]byte, n)
	z := len(LettersAndDigits)

	for i := 0; i < n; i++ {
		b[i] = LettersAndDigits[rand.Intn(z)]
	}
	return string(b)
}

func register(name, password string) (int64, error) {
	salt := randomString(20)
	digest := fmt.Sprintf("%x", sha1.Sum([]byte(salt+password)))

	createdAt := time.Now()
	res, err := db.Exec(
		"INSERT INTO user (name, salt, password, display_name, avatar_icon, created_at)"+
			" VALUES (?, ?, ?, ?, ?, ?)",
		name, salt, digest, name, "default.png", createdAt)
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	mycluster.CallAll("updateUser", User{
		ID:          id,
		Name:        name,
		Salt:        salt,
		Password:    digest,
		DisplayName: name,
		AvatarIcon:  "default.png",
		CreatedAt:   createdAt,
	})
	return id, err
}

func init() {
	cluster.Register("updateUser", updateUser)
	gob.Register(User{})
}
func updateUser(user User) {
	idToUsers.Store(user.ID, user)
	nameToUserID.Store(user.Name, user.ID)
}

// request handlers

func getInitialize(c echo.Context) error {
	logid := GetNextLogID()
	query := url.Values{"logid": {logid}}
	for i := 1; i <= 3; i++ {
		resp, err := http.Get(fmt.Sprintf("http://app%d/initialize/main?%s", i, query.Encode()))
		if err != nil {
			log.Panic(err)
		}
		defer resp.Body.Close()
		if _, err := ioutil.ReadAll(resp.Body); err != nil {
			log.Panic(err)
		}
	}
	return c.String(204, "")
}
func getInitializeMain(c echo.Context) error {
	StartLogger(c.QueryParam("logid"))

	db.MustExec("DELETE FROM user WHERE id > 1000")
	db.MustExec("ALTER TABLE user AUTO_INCREMENT = 11")
	db.MustExec("DELETE FROM image WHERE id > 1001")
	db.MustExec("DELETE FROM channel WHERE id > 10")
	db.MustExec("ALTER TABLE channel AUTO_INCREMENT = 11")
	db.MustExec("DELETE FROM message WHERE id > 10000")
	db.MustExec("DELETE FROM haveread")

	initializeMessageCache()
	initializeUserCache()
	initializeChannelCache()
	initializeHavereadCache()
	return c.String(204, "")
}

func getIndex(c echo.Context) error {
	userID := sessUserID(c)
	if userID != 0 {
		return c.Redirect(http.StatusSeeOther, "/channel/1")
	}

	return c.Render(http.StatusOK, "index", map[string]interface{}{
		"ChannelID": nil,
	})
}

func panicIf(err error) {
	if err != nil {
		log.Panic(err)
	}
}

const iconsDir = "/home/isucon/isubata/webapp/public/icons"

func init() {
	// extract image
	panicIf(os.MkdirAll(iconsDir, os.ModePerm))
	rows, err := db.Query("SELECT name, data FROM image")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var data []byte
		panicIf(rows.Scan(&name, &data))
		filename := iconsDir + "/" + name
		DumpImage(filename, data)
	}
}

type ChannelInfo struct {
	ID          int64     `db:"id"`
	Name        string    `db:"name"`
	Description string    `db:"description"`
	UpdatedAt   time.Time `db:"updated_at"`
	CreatedAt   time.Time `db:"created_at"`
}

var channels = []ChannelInfo{}
var channelMutex sync.RWMutex

func init() {
	cluster.Register("addChannel", addChannel)
	gob.Register(ChannelInfo{})
}
func addChannel(ch ChannelInfo) {
	channelMutex.Lock()
	defer channelMutex.Unlock()
	channels = append(channels, ch)
	sort.Slice(channels, func(i, j int) bool { return channels[i].ID < channels[i].ID })
}
func initializeChannelCache() {
	chs := []ChannelInfo{}
	err := db.Select(&chs, "SELECT * FROM channel")
	if err != nil {
		panic(err)
	}
	for _, ch := range chs {
		addChannel(ch)
	}
}

func getChannel(c echo.Context) error {
	user, err := ensureLogin(c)
	if user == nil {
		return err
	}
	cID, err := strconv.Atoi(c.Param("channel_id"))
	if err != nil {
		return err
	}
	channelMutex.RLock()
	defer channelMutex.RUnlock()

	var desc string
	for _, ch := range channels {
		if ch.ID == int64(cID) {
			desc = ch.Description
			break
		}
	}
	return c.Render(http.StatusOK, "channel", map[string]interface{}{
		"ChannelID":   cID,
		"Channels":    channels,
		"User":        user,
		"Description": desc,
	})
}

func getRegister(c echo.Context) error {
	return c.Render(http.StatusOK, "register", map[string]interface{}{
		"ChannelID": 0,
		"Channels":  []ChannelInfo{},
		"User":      nil,
	})
}

func postRegister(c echo.Context) error {
	name := c.FormValue("name")
	pw := c.FormValue("password")
	if name == "" || pw == "" {
		return ErrBadReqeust
	}
	userID, err := register(name, pw)
	if err != nil {
		if merr, ok := err.(*mysql.MySQLError); ok {
			if merr.Number == 1062 { // Duplicate entry xxxx for key zzzz
				return c.NoContent(http.StatusConflict)
			}
		}
		return err
	}
	sessSetUserID(c, userID)
	return c.Redirect(http.StatusSeeOther, "/")
}

func getLogin(c echo.Context) error {
	return c.Render(http.StatusOK, "login", map[string]interface{}{
		"ChannelID": 0,
		"Channels":  []ChannelInfo{},
		"User":      nil,
	})
}

func postLogin(c echo.Context) error {
	name := c.FormValue("name")
	pw := c.FormValue("password")
	if name == "" || pw == "" {
		return ErrBadReqeust
	}

	idi, ok := nameToUserID.Load(name)
	if !ok {
		return echo.ErrForbidden
	}
	id := idi.(int64)
	ui, _ := idToUsers.Load(id)
	user := ui.(User)

	digest := fmt.Sprintf("%x", sha1.Sum([]byte(user.Salt+pw)))
	if digest != user.Password {
		return echo.ErrForbidden
	}
	sessSetUserID(c, user.ID)
	return c.Redirect(http.StatusSeeOther, "/")
}

func getLogout(c echo.Context) error {
	sess, _ := session.Get("session", c)
	delete(sess.Values, "user_id")
	sess.Save(c.Request(), c.Response())
	return c.Redirect(http.StatusSeeOther, "/")
}

func postMessage(c echo.Context) error {
	user, err := ensureLogin(c)
	if user == nil {
		return err
	}

	message := c.FormValue("message")
	if message == "" {
		return echo.ErrForbidden
	}

	var chanID int64
	if x, err := strconv.Atoi(c.FormValue("channel_id")); err != nil {
		return echo.ErrForbidden
	} else {
		chanID = int64(x)
	}

	if err := addMessage(chanID, user.ID, message); err != nil {
		return err
	}

	return c.NoContent(204)
}

func jsonifyMessage(m Message) (map[string]interface{}, error) {
	res, _ := idToUsers.Load(m.UserID)
	u := res.(User)

	r := make(map[string]interface{})
	r["id"] = m.ID
	r["user"] = u
	r["date"] = m.CreatedAt.Format("2006/01/02 15:04:05")
	r["content"] = m.Content
	return r, nil
}

func getMessage(c echo.Context) error {
	userID := sessUserID(c)
	if userID == 0 {
		return c.NoContent(http.StatusForbidden)
	}

	chanID, err := strconv.ParseInt(c.QueryParam("channel_id"), 10, 64)
	if err != nil {
		return err
	}
	lastID, err := strconv.ParseInt(c.QueryParam("last_message_id"), 10, 64)
	if err != nil {
		return err
	}

	messages := queryMessages(chanID, lastID)

	response := make([]map[string]interface{}, 0)
	for i := len(messages) - 1; i >= 0; i-- {
		m := messages[i]
		r, err := jsonifyMessage(m)
		if err != nil {
			return err
		}
		response = append(response, r)
	}

	if len(messages) > 0 {
		mycluster.CallAll("insertHaveread", userID, chanID, messages[0].ID)
	}

	return c.JSON(http.StatusOK, response)
}

type Pair struct {
	userID int64
	chanID int64
}

var havereadMap *sync.Map = new(sync.Map)

func init() {
	cluster.Register("insertHaveread", insertHaveread)
}
func initializeHavereadCache() {
	havereadMap = new(sync.Map)
	rows, err := db.Query("SELECT user_id, channel_id, message_id FROM haveread")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		var p Pair
		var msgID int64
		err := rows.Scan(&p.userID, &p.chanID, &msgID)
		if err != nil {
			panic(err)
		}
		insertHaveread(p.userID, p.chanID, msgID)
	}
}
func insertHaveread(userID, chanID, msgID int64) {
	havereadMap.Store(Pair{userID, chanID}, msgID)
}

func queryHaveRead(userID, chID int64) int64 {
	res, ok := havereadMap.Load(Pair{userID, chID})
	if ok {
		return res.(int64)
	} else {
		return 0
	}
}

func getNumberOfMessages(chID int64) int64 {
	messagesMutex.RLock()
	defer messagesMutex.RUnlock()
	return int64(len(channelToMessages[chID]))
}
func fetchUnread(c echo.Context) error {
	userID := sessUserID(c)
	if userID == 0 {
		return c.NoContent(http.StatusForbidden)
	}

	time.Sleep(time.Second)

	channelMutex.RLock()
	defer channelMutex.RUnlock()

	resp := []map[string]interface{}{}

	for _, ch := range channels {
		lastID := queryHaveRead(userID, ch.ID)

		var cnt int64 = getNumberOfMessages(ch.ID) - lastID

		r := map[string]interface{}{
			"channel_id": ch.ID,
			"unread":     cnt}
		resp = append(resp, r)
	}

	return c.JSON(http.StatusOK, resp)
}

func getHistory(c echo.Context) error {
	chID, err := strconv.ParseInt(c.Param("channel_id"), 10, 64)
	if err != nil || chID <= 0 {
		return ErrBadReqeust
	}

	user, err := ensureLogin(c)
	if user == nil {
		return err
	}

	var page int64
	pageStr := c.QueryParam("page")
	if pageStr == "" {
		page = 1
	} else {
		page, err = strconv.ParseInt(pageStr, 10, 64)
		if err != nil || page < 1 {
			return ErrBadReqeust
		}
	}

	const N = 20
	var cnt int64
	cnt = getNumberOfMessages(chID)
	maxPage := int64(cnt+N-1) / N
	if maxPage == 0 {
		maxPage = 1
	}
	if page > maxPage {
		return ErrBadReqeust
	}

	messages := queryMessageHistory(chID, (page-1)*N+1, N)

	mjson := make([]map[string]interface{}, 0)
	for i := len(messages) - 1; i >= 0; i-- {
		r, err := jsonifyMessage(messages[i])
		if err != nil {
			return err
		}
		mjson = append(mjson, r)
	}

	channelMutex.RLock()
	defer channelMutex.RUnlock()

	return c.Render(http.StatusOK, "history", map[string]interface{}{
		"ChannelID": chID,
		"Channels":  channels,
		"Messages":  mjson,
		"MaxPage":   maxPage,
		"Page":      page,
		"User":      user,
	})
}

func getProfile(c echo.Context) error {
	self, err := ensureLogin(c)
	if self == nil {
		return err
	}

	channelMutex.RLock()
	defer channelMutex.RUnlock()

	userName := c.Param("user_name")
	uid, ok := nameToUserID.Load(userName)
	if !ok {
		return echo.ErrNotFound
	}
	res, _ := idToUsers.Load(uid.(int64))
	other := res.(User)

	return c.Render(http.StatusOK, "profile", map[string]interface{}{
		"ChannelID":   0,
		"Channels":    channels,
		"User":        self,
		"Other":       other,
		"SelfProfile": self.ID == other.ID,
	})
}

func getAddChannel(c echo.Context) error {
	self, err := ensureLogin(c)
	if self == nil {
		return err
	}

	channelMutex.RLock()
	defer channelMutex.RUnlock()

	return c.Render(http.StatusOK, "add_channel", map[string]interface{}{
		"ChannelID": 0,
		"Channels":  channels,
		"User":      self,
	})
}

func postAddChannel(c echo.Context) error {
	self, err := ensureLogin(c)
	if self == nil {
		return err
	}

	name := c.FormValue("name")
	desc := c.FormValue("description")
	if name == "" || desc == "" {
		return ErrBadReqeust
	}

	createdAt := time.Now()
	res, err := db.Exec(
		"INSERT INTO channel (name, description, updated_at, created_at) VALUES (?, ?, ?, ?)",
		name, desc, createdAt, createdAt)
	if err != nil {
		return err
	}
	lastID, _ := res.LastInsertId()
	addChannel(ChannelInfo{
		ID:          lastID,
		Name:        name,
		Description: desc,
		UpdatedAt:   createdAt,
		CreatedAt:   createdAt,
	})

	return c.Redirect(http.StatusSeeOther,
		fmt.Sprintf("/channel/%v", lastID))
}

func postProfile(c echo.Context) error {
	self, err := ensureLogin(c)
	if self == nil {
		return err
	}

	avatarName := ""
	var avatarData []byte

	if fh, err := c.FormFile("avatar_icon"); err == http.ErrMissingFile {
		// no file upload
	} else if err != nil {
		return err
	} else {
		dotPos := strings.LastIndexByte(fh.Filename, '.')
		if dotPos < 0 {
			return ErrBadReqeust
		}
		ext := fh.Filename[dotPos:]
		switch ext {
		case ".jpg", ".jpeg", ".png", ".gif":
			break
		default:
			return ErrBadReqeust
		}

		file, err := fh.Open()
		if err != nil {
			return err
		}
		avatarData, _ = ioutil.ReadAll(file)
		file.Close()

		if len(avatarData) > avatarMaxBytes {
			return ErrBadReqeust
		}

		avatarName = fmt.Sprintf("%x%s", sha1.Sum(avatarData), ext)
	}

	u, _ := idToUsers.Load(self.ID)
	user := u.(User)
	updated := false
	if avatarName != "" && len(avatarData) > 0 {
		filename := iconsDir + "/" + avatarName
		if !FileExists(filename) {
			mycluster.CallAll("DumpImage", filename, avatarData)
		}
		user.AvatarIcon = avatarName
		updated = true
	}

	if name := c.FormValue("display_name"); name != "" {
		user.DisplayName = name
		updated = true
	}
	if updated {
		mycluster.CallAll("updateUser", user)
	}

	return c.Redirect(http.StatusSeeOther, "/")
}

//var mycluster = cluster.NewCluster(20000, []string{"app1:20000", "app2:20000", "app3:20000"})
var mycluster = cluster.NewCluster(20000, "app1:20000", "app2:20000")

func init() {
	cluster.Register("DumpImage", DumpImage)
}

func DumpImage(filename string, data []byte) {
	if !FileExists(filename) {
		panicIf(ioutil.WriteFile(filename, data, 0644))
	}
}

func getIcon(c echo.Context) error {
	var name string
	var data []byte
	err := db.QueryRow("SELECT name, data FROM image WHERE name = ?",
		c.Param("file_name")).Scan(&name, &data)
	if err == sql.ErrNoRows {
		return echo.ErrNotFound
	}
	if err != nil {
		return err
	}

	mime := ""
	switch true {
	case strings.HasSuffix(name, ".jpg"), strings.HasSuffix(name, ".jpeg"):
		mime = "image/jpeg"
	case strings.HasSuffix(name, ".png"):
		mime = "image/png"
	case strings.HasSuffix(name, ".gif"):
		mime = "image/gif"
	default:
		return echo.ErrNotFound
	}
	return c.Blob(http.StatusOK, mime, data)
}

func tAdd(a, b int64) int64 {
	return a + b
}

func tRange(a, b int64) []int64 {
	r := make([]int64, b-a+1)
	for i := int64(0); i <= (b - a); i++ {
		r[i] = a + i
	}
	return r
}

func main() {
	e := echo.New()
	funcs := template.FuncMap{
		"add":    tAdd,
		"xrange": tRange,
	}
	e.Renderer = &Renderer{
		templates: template.Must(template.New("").Funcs(funcs).ParseGlob("views/*.html")),
	}
	e.Use(session.Middleware(sessions.NewCookieStore([]byte("secretonymoris"))))
	/*
		e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
			Format: "request:\"${method} ${uri}\" status:${status} latency:${latency} (${latency_human}) bytes:${bytes_out}\n",
		}))
	*/
	e.Logger.SetOutput(ioutil.Discard)

	e.GET("/initialize/main", getInitializeMain)
	e.GET("/initialize", getInitialize)
	e.GET("/", getIndex)
	e.GET("/register", getRegister)
	e.POST("/register", postRegister)
	e.GET("/login", getLogin)
	e.POST("/login", postLogin)
	e.GET("/logout", getLogout)

	e.GET("/channel/:channel_id", getChannel)
	e.GET("/message", getMessage)
	e.POST("/message", postMessage)
	e.GET("/fetch", fetchUnread)
	e.GET("/history/:channel_id", getHistory)

	e.GET("/profile/:user_name", getProfile)
	e.POST("/profile", postProfile)

	e.GET("add_channel", getAddChannel)
	e.POST("add_channel", postAddChannel)
	//e.GET("/icons/:file_name", getIcon)

	e.Start(":5000")
}
